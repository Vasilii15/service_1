const express = require('express');
const pg = require('pg');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const path = require('path');
const app = express();
const port = 8000;
const ejs = require('ejs');
var session = require('express-session');
var csrf = require('csurf');
var cookieParser = require('cookie-parser');
var compression = require('compression');
const multer  = require('multer');
var cors = require('cors');
var morgan = require('morgan');
const router = express.Router();

var http = require("http");
var url = require("url");

//WebSocket
const WebSocketServer = require('ws');
var webSocketServer = new WebSocketServer.Server({
    port: 8080
});
webSocketServer.on('connection', function(ws) {
    var id = Math.random();
    console.log("новое соединение " + id);
    ws.on('message', function(message) {
        console.log('получено сообщение ' + message);
        ws.send(message);
    });
    ws.on('close', function() {
        console.log('соединение закрыто ' + id);
    });
});


//Set Storage Engine
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

//Init Upload
const upload = multer({
    storage: storage,
    limits: {
        filesize: 100
    }
}).single('file');

app.use(cors());
app.use(compression());
//app.use(morgan('combined'));

var csrfProtection = csrf({ cookie: true });
var parseForm = bodyParser.urlencoded({ extended: false });

app.use(cookieParser());

app.set('trust proxy', 1)
app.use(session({ 
    secret: 'keyboard cat',
    saveUninitialized: true,
    resave: true,
    cookie: {
        maxAge: 24*60*60*1000
    }
}));

app.use(bodyParser.urlencoded({extended: true}));

const config = {
    user: 'userbd',
    database: 'project1_db',
    password: '123',
    host: 'localhost',
    port: 5432,
    max: 10,
    idleTimeoutMillis: 30000
};

const client = new pg.Client(config);
client.connect();

app.use('/public', express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

let services = [];

app.get('/', (req, res) => {
    if(req.session.login) res.redirect('/filter/All');
    else res.render('index', {});
});

app.get('/exit_user', (req,res) => {
    delete req.session.login;
    res.redirect('/');
});

app.use('/inputService/', async (req, res, next) => {
    console.log('req.files');
    console.log(req.files);
    
    if(req.session.csrfToken === req.body._csrf){
        next();
    } else {
        res.end();
    }
});

app.post('/inputService/', async (req, res) => {
    upload(req, res, (err) => {
        if(err){
            res.send('err');
        } else {
            //console.log(req.body);
            //console.log(req.file);
            next();
        }
    });
});

app.post('/inputService/', parseForm, csrfProtection, async (req, res) => {
    upload(req, res, async(err) => {
        if(err){
            res.send('err');
        } else {
            const query = "INSERT INTO dataservices (service, users, phone, name, img) VALUES ($1, $2, $3, $4, $5)";
            let result = await client.query(query, [req.body.service, req.body.login, req.body.phone, req.body.name, reg.file.filename]);
            if(result.rows){
                res.send('ok');
            } else res.send('err');
        }
    });
});

app.use('/input_service', async (req, res, next) => {
    if(req.session.login){
        next();
    } else res.redirect('/');
});
app.use('/input_service', async (req, res, next) => {
    const query = "SELECT * FROM users_table WHERE users=$1";
    let result = await client.query(query, [req.session.login]);
    result.rows[0].type_user.replace(/\s+/g, '') === 'admin' ? next() : res.redirect('/404');
});
app.get('/input_service', csrfProtection, async (req, res) => {
    services = await client.query('SELECT service FROM service');
    services = services.rows;
    const csrfToken = req.csrfToken();
    req.session.csrfToken = csrfToken;
    res.render('service_entry', { services, csrfToken: csrfToken });
});
app.get('/404', async (req,res) => {
    res.render('404', {});
})

app.get('/filter/:param', async (req, res) => {
    if(!req.session.login) {
        res.redirect('/');
    } else {
        const filter = req.params.param;
        let query = '';
        let data = '';
        if(filter == 'All') {
            query = "SELECT * FROM dataservices";
            data = await client.query(query);
        } 
        else {
            query = "SELECT * FROM dataservices WHERE service=$1";
            data = await client.query(query, [filter]);
        }
        services = await client.query('SELECT service FROM service');
        services = services.rows;
        res.render('render_service', { services, data: data.rows });
    }    
})

app.all('/form1/', async (req, res) => {
    if(req.body.type == 'login') {
        const query = "SELECT * FROM users_table WHERE users=$1";
        const result = await client.query(query, [req.body.login]);
        if(result.rows[0]) {
            let pass = result.rows[0].password.replace(/\s+/g, '');
            if(req.body.pass == pass) {
                req.session.login = req.body.login;
                res.send('ok');
            } else {
                res.send('not_pass');
            }
        } else res.send('not_login');
    }
    if(req.body.type == 'check') {
        let query = "SELECT * FROM users_table WHERE users=$1";
        let result = await client.query(query, [req.body.login]);
        console.log(result.rows);
        if(!result.rows[0]) {
            query = "INSERT INTO users_table (users, password, type_user) VALUES ($1, $2, 'user')";
            result = await client.query(query, [req.body.login, req.body.pass]);
            result ? res.send('ok') : res.send('error');
        } else res.send('exist');
    }
});

app.listen(port, () => {});
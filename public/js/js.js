if(window.location.pathname.indexOf('/input_service') + 1) {
    document.querySelector('.input-service').addEventListener('click', function(e){
        let target = e. target;
        if(!target.classList.contains('btn__list')) return false;
        let elem = target.parentElement.parentElement;
        elem.querySelector('ul').classList.toggle('active');
        elem.addEventListener('click', checkValue);
        document.querySelector('.btn__rec-service').addEventListener('click', recordService);
    });
} else if(window.location.pathname.indexOf('/filter/') + 1){

} else if(window.location.pathname.indexOf('/404') + 1) {

} else {
    /*Есть ли в памяти Логи и пароль*/
    /*if(getCookie('userLogin')) {
        const login = getCookie('userLogin');
        const pass = getCookie('userPassword');
        document.querySelector('.form__input .login').value = login;
        document.querySelector('.form__input .password').value = pass;
    }*/

    /*Форма Входа*/
    document.querySelector('.form__input').addEventListener('submit', function(e){
        e.preventDefault();
        let login = document.querySelector('.login').value;
        let pass = document.querySelector('.password').value;
        if(!controlLogin()) return false;
        $.ajax({
            url: '/form1/',
            type: 'POST',
            data: {
                'login': login,
                'pass': pass,
                'type': 'login'
            },
        })
        .done(function(msg) {
            if(msg == 'ok'){
                let login = document.querySelector('.form__input .login').value;
                let pass = document.querySelector('.form__input .password').value;
                //document.cookie = "userLogin=" + login;
                //document.cookie = "userPassword=" + pass;
                document.querySelector('.input_project').classList.add('shadow');
                document.querySelector('.choice').classList.add('view');
            } else if(msg == 'not_login'){
                let err = 'Данный пользователь не зарегистрирован';
                let errorTemplate = '<div class="alert alert-danger">'+ err +'</div>';
                $('.form__input').append(errorTemplate);
            } else if(msg == 'not_pass'){
                let err = 'Не верный пароль';
                let errorTemplate = '<div class="alert alert-danger">'+ err +'</div>';
                $('.form__input').append(errorTemplate);
            } else console.log(msg);
        })
        .fail(function(msg) {
            console.log(msg);
        })
    })

    /*Форма Регистрации*/
    document.querySelector('.btn__check').addEventListener('click', function(e){
        e.preventDefault();
        let login = document.querySelector('.login').value;
        let pass = document.querySelector('.password').value;
        if(!controlLogin()) return false;
        $.ajax({
            url: '/form1/',
            type: 'POST',
            data: {
                'login': login,
                'pass': pass,
                'type': 'check'
            },
        })
        .done(function(msg) {
            if(msg == 'ok'){
                let message = 'Пользователь успешно зарегистрирован';
                let succesTemplate = '<div class="alert">'+ message +'</div>';
                $('.form__input').append(succesTemplate);
                return;
            } else if(msg == 'exist'){
                let err = 'Пользователь с таким именем существует';
                let errorTemplate = '<div class="alert alert-danger">'+ err +'</div>';
                $('.form__input').append(errorTemplate);
                return;
            } else console.log(msg);
        })
        .fail(function(msg) {
            console.log(msg);
        })
    });
}

/*Ввод услуги*/
let choiceInput = document.querySelectorAll('.choice__input')
for(let i = 0; i < choiceInput.length; i++){
    choiceInput[i].addEventListener('click', function(){
        location.href = '/input_service';
    });
}

/*Обработчик на просмотр услуг*/
let choiceView = document.querySelectorAll('.choice__view');
for(let i = 0; i < choiceView.length; i++){
    choiceView[i].addEventListener('click', function(){
        location.href = '/filter/All';
    });
}

/*Кнопка выхода*/
let btnExit = document.querySelectorAll('.btn__exit');
for(let i = 0; i < btnExit.length; i++){
    btnExit[i].addEventListener('click', function(){
        location.href = '/exit_user';
    });
}

document.querySelector('body').addEventListener('click', function(){
	if(document.querySelector('.alert')) {
        let alert = document.querySelector('.alert');
        alert.parentElement.removeChild(alert);
	};
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function checkValue(e){
    e.preventDefault();
    if(e.target.tagName !== 'A') return false;
    let value = e.target.textContent;
    let elem = e.target.parentElement.parentElement;
    elem.classList.remove('active');
    elem.parentElement.querySelector('.block__row-value').textContent = value;
}

function controlLogin(){
    let login = document.querySelector('.login').value;
    let pass = document.querySelector('.password').value;
    let err = '';
    let err_s = 0;
    document.querySelector('.form__input').addEventListener('keydown', function(e){
        let target = e.target;
        if(target.classList.contains('error_input')) target.classList.remove('error_input');
    })
    if(login == '') {
        document.querySelector('.login').classList.add('error_input');
        err = err + 'Поле Логин';
        err_s = err_s + 1;
    }
    if(pass == '') {
        document.querySelector('.password').classList.add('error_input');
        err_s > 0 ? err = err + ' и поле Пароль' : err = err + 'Поле Пароль';
        err_s = err_s + 1;
    }
    if(err_s > 0) {
        err_s > 1 ? err = err + ' не заполнены' : err = err + ' не заполнено';
        let errorTemplate = '<div class="alert alert-danger">'+ err +'</div>';
        $('.form__input').append(errorTemplate);
        return false;
    }
    return true;
}

function recordService(e){
    e.preventDefault();
    let service = document.querySelector('.input-service .block__service').textContent;
    service = service.replace(/\s+/g, '');
    let phone = document.querySelector('.input-service .block__phone').value;
    let name = document.querySelector('.input-service .block__name').value;
    let login = localStorage.getItem('user');

    let token = document.querySelector('.block__token').value;

    let file_data = $('.block__file').prop('files')[0];
    console.log(file_data);
    let form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('service', service);
    form_data.append('phone', phone);
    form_data.append('name', name);
    form_data.append('login', login);
    form_data.append('_csrf', token);

    console.log(form_data);

    if(!service || !phone || !name) {
        let err = 'Заполните все поля';
        let errorTemplate = '<div class="alert alert-danger">'+ err +'</div>';
        $('.input-service .block').append(errorTemplate);
        return false;
    }
    $.ajax({
        url: '/inputService/',
        type: 'POST',
        processData: false,
        contentType: false,
        data: form_data
    })
    .done(function(msg) {
        if(msg == 'ok'){
            let message = 'Данные успешно введены';
            let succesTemplate = '<div class="alert">'+ message +'</div>';
            $('.input-service .block').append(succesTemplate);
            return;
        } else console.log(msg);
    })
    .fail(function(msg) {
        console.log(msg);
    })
}

/*WebSocket*/
// создать подключение
var socket = new WebSocket("ws://localhost:8080");

// отправить сообщение
if(document.querySelector('.btn__w-socket')){
    document.querySelector('.btn__w-socket').addEventListener('click', function(e){
        e.preventDefault();
        var outgoingMessage = 'Message Client';
        socket.send(outgoingMessage);
    });
}

// обработчик входящих сообщений
socket.onmessage = function(event) {
    let incomingMessage = event.data;
    showMessage(incomingMessage);
};

// показать сообщение 
function showMessage(message) {
    /*let messageElem = document.createElement('div');
    messageElem.appendChild(document.createTextNode(message));
    document.getElementById('subscribe').appendChild(messageElem);*/
    console.log(message);
}
const path = require('path');

module.exports = {
    entry: './server.js',
    output:{
        path: path.resolve(__dirname, 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['env']
                        }
                    },
                    {
                        loader: "ejs-webpack-loader",
                        options: {
                            htmlmin: true
                        }
                    }
                ]
            }
        ]
    }
}